<?php

function miAccionPost()
{
    echo 'Funcion POST';
}

function miAccionGet()
{
    echo 'Funcion GET';
}

function miAccionDefault() {
    echo 'Funcion DEFAULT';
}

function miAccionPostAlternativa()
{
    echo 'Funcion POST alternativa';
}