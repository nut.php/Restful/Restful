<?php

/**
 * RestfulTest
 *
 * @group Test
 */
class RestfulTest extends \PHPUnit_Framework_TestCase
{
    private static $http;

    public static function setUpBeforeClass() {
        self::$http = new CurlRestful([
            'url' => URL_API,
        ]);
    }
        
    public function testRequestUsingHttpVerbAssociatedWithClassFunction()
    {
        $html = self::$http->post();
        $this->assertContains('METODO POST' , strtoupper($html));
    }
    
    public function testRequestUsingHttpVerbAssociatedWithGlobalFunction()
    {
        $html = self::$http->get();
        $this->assertContains('FUNCION GET' , strtoupper($html));
    }
        
    public function testRequestUsingHttpVerbAssociatedWithUndefinedFunction()
    {
        $html = self::$http->put();
        $this->assertContains('EXCEPTION' , strtoupper($html));
    }
    
    public function testRequestUsingHttpVerbNotAssociatedWithFunction()
    {
        $html = self::$http->delete();
        $this->assertContains('FUNCION DEFAULT' , strtoupper($html));
    }
    
    public function testRequestUsingHttpVerbNotAssociatedWithFunctionAndError()
    {
        $html = self::$http->patch('/no-default.php');
        $this->assertContains('EXCEPTION' , strtoupper($html));
    }
    
    public function testRequestUsingHttpVerbAssociatedWithGlobalFunctionAlternativeOn()
    {
        $html = self::$http->post('/preferred.php', ['parcial' => 'true']);
        $this->assertContains('FUNCION POST ALTERNATIVA' , strtoupper($html));
    }    
    
    public function testRequestUsingHttpVerbAssociatedWithGlobalFunctionAlternativeOff()
    {
        $html = self::$http->post('/preferred.php');
        $this->assertContains('METODO POST' , strtoupper($html));
    }
    public function testRequestUsingHttpVerbNotAssociatedWithFunctionAlternativeOn()
    {
        $html = self::$http->get('/preferred.php', ['parcial' => 'true']);
        $this->assertContains('FUNCION GET' , strtoupper($html));
    }

    public function testRequestUsingHttpVerbNotAssociatedWithFunctionAlternativeOff()
    {
        $html = self::$http->get('/preferred.php');
        $this->assertContains('FUNCION GET' , strtoupper($html));
    }    
    
    public function testRequestUsingHttpVerbNotAssociatedWithFunctionAlternativeOnAndError()
    {
        $html = self::$http->patch('/no-default.php');
        $this->assertContains('EXCEPTION' , strtoupper($html));
    }
    
    public function testRequestUsingHttpVerbNotAssociatedWithFunctionAlternativeOffAndError()
    {
        $html = self::$http->patch('/no-default.php');
        $this->assertContains('EXCEPTION' , strtoupper($html));
    }
}

