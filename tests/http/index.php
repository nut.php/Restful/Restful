<?php

require ('../classes/Http.php');
require ('../helpers/http.php');
require('../../src/Nut/Restful/Restful.php');

$restful = new \Nut\Restful\Restful(new Http);
$restful
    ->get('miAccionGet')            //La funci�n asociada no existe como m�todo de la clase, pero si como funcion global
    ->post('miAccionPost')            
    ->put('miAccionPut')            //La funcion asociada no existe en ning�n lado
    ->default('miAccionDefault');   //La funcion asociada ser� ejecutada por cualquier verbo HTTP no definido