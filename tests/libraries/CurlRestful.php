<?php
class CurlRestful
{
    private $url;
    private $parseUrl;
    private $data;
    private $options;

    public function __construct($config = [])
    {
        /** equivalente a ($parse_url = $config['url'] ?? []) en PHP 7.0+ */
        $parse_url = isset($config['url']) ? parse_url($config['url']) : [];
        $config_base = [
            'scheme' => 'http',
            'server' => 'localhost',
            'port' => NULL,
            'pass' => NULL,
            'path' => NULL,
            'query' => NULL,
            'fragment' => NULL
        ];

        $this->infoUrl = array_merge($config_base, $config, $parse_url);
    }

    public function unparseUrl()
    {
        $parsed_url = $this->infoUrl;
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
        $pass     = ($user || $pass) ? "$pass@" : ''; 
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
        return "$scheme$user$pass$host$port$path$query$fragment"; 
    }

    private static function isUrlValid($string) {
        return (Bool)(filter_var($string, FILTER_VALIDATE_URL));
    }

    public function initialize($parameters)
    {
        $path = array_shift($parameters);
        if (is_array($path)) {            
            $this->data = $path;
        } else {
            $this->data = array_shift($parameters) ?: [];
        }
        $this->options = array_shift($parameters) ?: [];
        $this->setUrl($path);
    }

    public function setUrl($path)
    {
        if (self::isUrlValid($path)) {
            $this->url = $path;
        } else {
            $this->url = trim($this->unparseUrl(), '/') 
                       . preg_replace('/\/{2,}/', '/', '/' . $path);
        }
    }
    
    public function send($method)
    {
        $method = strtoupper($method);
        $ch = curl_init($this->url);   
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

        if ($method == 'HEAD') {
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, true);
        } else {   
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($this->data));
        }

        foreach ($this->options as $const => $option) {
            curl_setopt($ch, $const, $option);
        }

		$response = curl_exec($ch);
		curl_close($ch);
        return $response;
    }
    
    public function __call($method, $arguments)
    {
        $this->initialize($arguments);
        return $this->send($method);
    }
}