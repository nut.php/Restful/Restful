<?php

require('./libraries/CurlRestful.php');

foreach (glob('helpers/*.php') as $file) {
    require ($file);
}

function correctDirectorySeparators($path) {
    return str_replace(['\\', '/'], DIRECTORY_SEPARATOR, $path);
}

function autoloadClass($class_name) {
    $class_file = correctDirectorySeparators('classes/' . $class_name . '.php');
    if (file_exists($class_file)) {
        require ($class_file);
    }
}

spl_autoload_register('autoloadClass');