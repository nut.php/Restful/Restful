<?php

namespace Nut\Restful;

use Exception;

/**
 * Controlador RestFul para CodeIgniter
 *
 * @package         Nut
 * @subpackage      Restful
 * @category        Libraries
 * @author          Neder Alfonoso Fandiño Andrade <neafand@gmail.com>
 * @license         CC-BY-SA-4.0
 * @link            
 * @version         1.0.0
 * @description     Inicialmente se toma como patron base la libreria REST_controller de Phil Sturgeon, Chris Kacerguis
 *                  
 */
class Restful
{

	/**
	 * @var object
	 */
	private $class = null;

	/**
	 * @var array
	 */
	private $callbacks = [];

	/**
	 * @var bool
	 */
	private $isExecutable = FALSE;

	/**
	 * @var bool
	 */
	private $alternate = FALSE;
	
	/**
	 * @var bool
	 */
	private $alternative = FALSE;
	
	/**
	 * @param object $class
	 */
	public function __construct($class = null)
	{
		$this->class = $class;
	}

	/**
	 * @param string $method
	 * @param callable $callback
	 * @param array $arguments
	 * @return self
	 */
	public function associateMethodWithCallback($method, $callback, $arguments = [])
	{
		$this->callbacks[$this->alternate][$method]['callback'] = $callback;
		$this->callbacks[$this->alternate][$method]['arguments'] = $arguments;
        return $this;
	}

	/**
	 * @return string
	 */
	public function getNameInputMethod()
	{
		$input_method = strtolower($_SERVER['REQUEST_METHOD']);

		if ($input_method == 'get') {
			$fake_method = filter_input(INPUT_GET, '_method');
		} else {
			$fake_method = filter_input(INPUT_POST, '_method');
		}

		return strtolower($fake_method ?: $input_method);
	}

	/**
	 * @param string $method
	 * @return bool
	 */
	public function existCallbackForMethod($method)
	{
		return key_exists($method, $this->callbacks[$this->alternative])
			   || key_exists($method, $this->callbacks[FALSE]);
	}

	/**
	 * Undocumented function
	 *
	 * @param string $method
	 * @return mixed
	 */
	public function getCallbackForMethod($method)
	{
		if (isset($this->callbacks[$this->alternative][$method])) {
			$callback = $this->callbacks[$this->alternative][$method];
		} else {
			$callback = $this->callbacks[FALSE][$method];
		}
		return $callback;
	}

	/**
	 * @return mixed
	 */
	public function getCallbackAccordingInputMethodServer()
	{
		$method = $this->getNameInputMethod();   
		if ($this->existCallbackForMethod($method)) {

		} elseif ($this->existCallbackForMethod('default')) {
			$method = 'default';
		} else {
			throw new Exception("El verbo `$method` no esta asociado a ninguna accion");			
		}
        return $this->getCallbackForMethod($method);
	}

	/**
	 * @return array
	 */
	public function getArguments($arguments = NULL)
	{
		$arguments = $arguments ?:[];	
		if ( ! is_null($this->id)) {
			array_unshift($arguments, $this->id);
		}

		return $arguments;
	}

	/**
	 * @return callable
	 */
	public function getCallable($method)
	{
		$methodClass = [$this->class, $method];

		if (is_callable($methodClass)) {
			$callable = $methodClass;
		} else {
			$callable = $method;
		}

		if (!is_callable($callable)) {
			$method = strtoupper($this->getNameInputMethod());
			$callable = var_export($callable, true);
			throw new Exception("Error: no existe la accion $callable asociada al verbo `$method`");
		}

		return $callable;
	}

	/**
	 *
	 * @param mixed $id
	 * @return self
	 */
	public function withId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 *
	 * @param mixed $condition
	 * @return self
	 */
	public function alternate($condition)
	{
		$this->alternate = TRUE;
		if (is_bool($condition)) {
			$this->alternative = $condition;
		} else {
			$callable = $this->getCallable($condition);
			$this->alternative = $callable();
		}
		return $this;
	}

	/**
	 * @param string $name
	 * @param array $arguments
	 * @return self
	 */
	public function __call($name, $arguments)
	{
		$this->isExecutable = TRUE;
		$callback = array_shift($arguments);
		$parameters = array_shift($arguments);
		return $this->associateMethodWithCallback($name, $callback, $parameters);
	}
	
	/**
	 *
	 * @param mixed $id
	 * @return self
	 */
	public function __invoke($id)
	{
		return $this->withId($id);
	}

	/**
	 * @return mixed
	 */
	public function execute()
	{
		if ($this->isExecutable) {
			$this->isExecutable = FALSE;
			$callback = $this->getCallbackAccordingInputMethodServer();
			$callable = $this->getCallable($callback['callback']);
			$arguments = $this->getArguments($callback['arguments']);		
			return call_user_func_array($callable, $arguments);
		}
	}

	public function __destruct()
	{
		$this->execute();
	}

}
